#!/usr/bin/python


#function to read preferces and return a list
#0-23 are hours of the day
#element 24 and 25 are on / off coefficients
def ReadPrefs(PrefFile):
    fh = open(PrefFile, "rb");

    Prefs = []
    for line in fh:
        line = float(line.strip())
        Prefs.append( line )
    fh.close()
    return Prefs
#end def ReadPrefs

#function to write the (modified) ouptut from ReadPrefs
def WritePrefs(PrefFile, Prefs):
    fh = open(PrefFile, 'w')
    #itterate through each pref, saving a line at a time
    for i in range(len(Prefs)):
        fh.write(str(Prefs[i]) + '\n')

    fh.close()
#end def WritePrefs


HeatPrefs = []
HeatPrefs = ReadPrefs("heat.prefs")
#print HeatPrefs
#print HeatPrefs[0]
HeatPrefs[0] = HeatPrefs[0] + 1 #this confirms the data is float not str


WritePrefs('heat.prefs', HeatPrefs)

#del HeatPrefs[:]
