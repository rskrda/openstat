import smbus

i2cport1 = smbus.SMBus(1)
i2cdata = []
addr = 0x28
i2cdata = i2cport1.read_i2c_block_data(addr, 0)

#remove bit 6:
b0 = i2cdata[0]
b0 = b0 & ~(1<<7)
b0 = b0 & ~(1<<6)

humidity = (b0 * 256.00  + i2cdata[1]) / 16384.00 * 100.00



#temperature, B2 is fine, B3 needs 2 >> bit shift, significance matters
b3 = i2cdata[3]
b3 = b3 & ~(1<<0)
b3 = b3 & ~(1<<1)


TempCelcius = ((((i2cdata[2] * 64.00) + ( b3 / 4.00)) / 16384.00 ) * 165.00) - 40

#TempFarenheit = TempCelcius*9/5 + 32

#humidity bit banging
#remove bit 6:

#print "%x" % b1
print str(round(humidity, 2)) + " % "  + str(round(TempCelcius, 2)) + " C " 
#enter i2c bit banging. The results are two 14 bit sections in 4 bytes:
#
#s s h h  h h h h  h h h h  h h h h  t t t t  t t t t  t t t t  t t x x
#
#those are the actual positions. S is a status always set to 10 (ignore)
# h is 14 bit humidity, t is 14 bit temperature, x is random noise (ignore)
