#!/usr/bin/python -tt
import math
import sys
import subprocess
import time

def logger():
    C1tmp = subprocess.check_output('/usr/bin/python cca2.py', shell=True)
    C2tmp = subprocess.check_output('/usr/bin/python cca2-i2c3.py', shell=True)
    tmp = subprocess.check_output('./ads1015', shell=True) 

    C2 = str(C2tmp)
    C2 = C2.rstrip(' \n ')
    C1 = str(C1tmp)
    C1 = C1.rstrip(' \n ')



    Vout = float(tmp)
    Vin = 3.3
    c1 = -1 #-1 degree manual correction
    R2 = 9920 #10kohm R2
    T0 = 298.15 #25*C in Kelvin (273.15+25)
    Beta = 3977.00 #from thermistor datasheet


    #voltage to resistance
    R1 = (R2 * Vin / Vout) - R2


    #formula from http://psas.pdx.edu/RocketScience/Thermistors.pdf
    temp = (Beta * T0) / (Beta + T0 * math.log(R1/R2))


    print C1 + " " + C2 + " " + str((temp - 273.15 + c1)) + " C"
#end logger

def main():
    while 1:
        logger()
        time.sleep(300)
    #end while

#end main

if __name__ == '__main__':
    main()
