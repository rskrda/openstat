// from  http://openlabtools.eng.cam.ac.uk/Resources/Datalog/RPi_ADS1115/
#include <stdio.h>
#include <fcntl.h>     // open
#include <inttypes.h>  // uint8_t, etc
#include <string.h>    //strcpy, strcat
#include <linux/i2c-dev.h> // I2C bus definitions

int main(int argc, char *argv[]) {

  if (argc!=4) 
  {   
    printf("Usage: %s <AIN port> <ADS1015 address> <i2c bus>\n", argv[0]);
    printf("Valid options for <AIN port> are 0, 1, 2 or 3. This is the port on the ADS1015\n");
    printf("Valid options for <ADS1015 address> are 72 or 73 decimal that translate to 0x48 or 0x49. This is the address of the ADS1015\n");
    printf("Valid options for <i2c bus> are 1 or 2. This is the i2c bus number\n\n");
    printf("Options are not checked so use with caution\n");
    printf("Example: ./ads1015 1 72 2\n\n");
    return -1; 
  } 
  int ain = atoi(argv[1]);  //input AIN<0-4> port requested
 
  int ADS_address = atoi(argv[2]);   // Address of our device on the I2C bus, default 0x48/72d
  int I2CFile;
  
  uint8_t writeBuf[3];      // Buffer to store the 3 bytes that we write to the I2C device
  uint8_t readBuf[2];       // 2 byte buffer to store the data read from the I2C device
  
  int16_t val;              // Stores the 16 bit value of our ADC conversion
 
  char busname[16] = {0};
  strcpy (busname, "/dev/i2c-");
  strcat (busname, argv[3]);
 
  // These three bytes are written to the ADS1015 to set the config register and start a conversion 
  // 4.096 volts is used on the gain amp. The formula is ainX / 32767 * 4.096 = milivolts
  // Milivolts should be directly measurable with a multimeter on ground and ainX
  //the bit numbers are written vertically below
  writeBuf[0] = 0b00000001;

  switch(ain)
  {
    case 0:
      writeBuf[1] = 0b11000011;
      break;
    case 1: 
      writeBuf[1] = 0b10110011;
      break;
    case 2: 
      writeBuf[1] = 0b11010011;
      break;
    case 3: 
      writeBuf[1] = 0b11110011;
      break;
                    //11111100
                    //54321098
    default: 
      printf("The valid range is 0 to 3, exiting...");
      return -1;
      break;
  }
  writeBuf[2] = 0b10000011;
                //76543210
  //These are all configured to be the defaults from TI's ads1013.pdf pg. 16

  I2CFile = open(busname, O_RDWR);     // Open the I2C device, like "/dev/i2c-2"
 
  ioctl(I2CFile, I2C_SLAVE, ADS_address);   // Specify the address of the I2C Slave to communicate with


  //init read buffer 
  readBuf[0] = 0;        
  readBuf[1] = 0;        
      
  // Write writeBuf to the ADS1015, the 3 specifies the number of bytes we are writing,
  // this begins a single conversion
  write(I2CFile, writeBuf, 3);  

  // Wait for the conversion to complete, this requires bit 15 to change from 0->1
  while ((readBuf[0] & 0x80) == 0)  // readBuf[0] contains 8 MSBs of config register, AND with 10000000 to select bit 15
  {
    read(I2CFile, readBuf, 2);    // Read the config register into readBuf
    //infinite loop risk
  }

  
  //printf("after config write: debug: %d %d\n", readBuf[0], readBuf[1]);

  writeBuf[0] = 0;                  // Set pointer register to 0 to read from the conversion register
  write(I2CFile, writeBuf, 1);
 
  int i;
  read(I2CFile, readBuf, 2);        // Read the contents of the conversion register into readBuf

  val = readBuf[0] << 8 | readBuf[1];   // Combine the two bytes of readBuf into a single 16 bit result 

  //printf("after conversion write debug: %d %d\n", readBuf[0], readBuf[1]);
 
  //printf("Voltage Reading %f (V) \n", (float)val*4.096/32767.0);    // Print the result to terminal, first convert from binary value to mV
  //printf("%f\n", (float)val);
  printf("%f\n", (float)val/32767.0*4.096);    // Print the result to terminal, first convert from binary value to mV

  close(I2CFile);
  
  return 0;

}
