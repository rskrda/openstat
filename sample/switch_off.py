import Adafruit_BBIO.GPIO as GPIO
import time
 
pins = ["P9_22", "P9_26", "P9_42", "P9_11", "P9_13", "P9_21", "P9_25", "P9_41"]

for pin in pins:
    print pin
    GPIO.setup(pin, GPIO.OUT)
    #GPIO.output(pin, GPIO.HIGH)
    #time.sleep(1.00)
    GPIO.output(pin, GPIO.LOW)
    time.sleep(0.20)

time.sleep(10.00)

GPIO.cleanup()
