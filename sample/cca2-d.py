#enter i2c bit banging. The results are two 14 bit sections in 4 bytes:
#   
#s s h h  h h h h  h h h h  h h h h  t t t t  t t t t  t t t t  t t x x
#   
#those are the actual positions. S is a status always set to 10 (ignore)
# h is 14 bit humidity, t is 14 bit temperature, x is random noise (ignore)


# this if structure and LastPoll is designed to prevent CCA2 from being
# polled too frequently. In testing, frequent polling leads to inaccurate
# results which could lead to unexepcted behavior
# the documentation states once every 7 seconds is ok, so I chose 8 seconds

import smbus
#init variabels
i2cport1 = smbus.SMBus(2)
i2cdata = []
addr = 0x28 #cca2 defaults 0x28 on i2cbus 2
i2cdata = i2cport1.read_i2c_block_data(addr, 0)

#temperature, B2 is fine, B3 needs 2 >> bit shift, significance matters
b3 = i2cdata[3]
b3 = b3 & ~(1<<0)
b3 = b3 & ~(1<<1)

TempCelcius = ((((i2cdata[2] * 64) + ( b3 / 4.00)) / 16384.00) * 165.00) - 48

#TempFarenheit = TempCelcius*9/5 + 32

#humidity bit banging
#remove bit 6:
b0 = i2cdata[0]
b0 = b0 & ~(1<<7)
b0 = b0 & ~(1<<6)

#b1 = i2cdata[0]
#mask = ~(1 << 6)
#b1 = b1 & mask
Humidity = ((b0 * 256.00  + i2cdata[1]) / 16384.00)  * 100 

print TempCelcius
print Humidity
