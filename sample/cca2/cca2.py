#!/usr/bin/python -tt
import math
import sys


def main():

    Vout = float(sys.argv[1])
    Vin = 3.3
    c1 = -1 #-1 degree manual correction
    R2 = 9920 #10kohm R2
    T0 = 298.15 #25*C in Kelvin (273.15+25)
    Beta = 3977.00 #from thermistor datasheet


    #voltage to resistance
    R1 = (R2 * Vin / Vout) - R2


    #formula from http://psas.pdx.edu/RocketScience/Thermistors.pdf
    temp = (Beta * T0) / (Beta + T0 * math.log(R1/R2))


    print "input voltage at " + sys.argv[1] + " volts"
    print "Resistor 1 is calculated to be " + str(R1) + " ohms"
    print "thermistor temperature is " + str((temp - 273.15 + c1)) + " celcius"
#end main

if __name__ == '__main__':
    main()
