// from  http://openlabtools.eng.cam.ac.uk/Resources/Datalog/RPi_ADS1115/
#include <stdio.h>
#include <fcntl.h>     // open
#include <inttypes.h>  // uint8_t, etc
#include <string.h>    //strcpy, strcat
#include <linux/i2c-dev.h> // I2C bus definitions

int main(int argc, char *argv[]) {

  if (argc!=2) 
  {   
    printf("Usage: %s <i2c bus>\n", argv[0]);
    printf("Valid options for <i2c bus> are 1 or 2. This is the i2c bus number\n\n");
    printf("Options are not checked so use with caution\n");
    printf("Example: ./cca2 2\n\n");
    return -1; 
  } 
  int ADS_address = 0x28;   // Address of our device on the I2C bus, default 0x48/72d
  int I2CFile;
  
  
  char busname[16] = {0};
  strcpy (busname, "/dev/i2c-");
  strcat (busname, argv[1]);

  if ((I2CFile = open(busname, O_RDWR)) <0 )  {     // Open the I2C device, like "/dev/i2c-2"
    printf("Failed to open the i2c bus\n");
    return(-1);
  }

  if (ioctl(I2CFile, I2C_SLAVE, ADS_address) <0 ) {
    printf("Failed to acquire bus access and/or talk to slave.\n");
    return(-1);    
  }

  //init read buffer 
  uint8_t raw[4];       // 2 byte buffer to store the data read from the I2C device
  raw[0] = 0x24;
  raw[1] = 0x62;
  raw[2] = 0xD3;
  raw[3] = 0x48;
   
  //printf("reading data\n"); 
//  read(I2CFile, raw, 4);        // Read the contents of the conversion register into readBuf

  printf("raw output: %x %x %x %x\n", raw[0], raw[1], raw[2], raw[3]);

  close(I2CFile);

  uint16_t value_temp;

  float hum = 0;
  float temp = 0;

  value_temp = raw[0];
  value_temp = (value_temp & 0x3F) << 8;
  value_temp = value_temp | raw[1];
  hum = value_temp;
  hum = hum / 163.84;

  value_temp = raw[2];
  value_temp = value_temp << 8;
  value_temp = value_temp | raw[3];
  value_temp = value_temp & 0xFFFC;
  temp = value_temp;
  temp = ((temp / 65536) * 165) - 40; 

  printf("hum: %.2f temp: %.2f\n", hum, temp);
 

 
  return 0;

}
