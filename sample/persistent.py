import pickle

persistent = {}
persistent['IPAddress'] = '192.168.1.62'
persistent['Gateway'] = '192.168.1.1'
persistent['UserTemp'] = 21
persistent['NightStart'] = 23
persistent['NightEnd'] = 7

print persistent

f = open('tmp.pckl', 'w')
pickle.dump(persistent, f)
f.close()
    
try: 
    f = open('tmp.pckl')
    tmppers = pickle.load(f)
    f.close()
except IOError:
    print "couldn't open file"

print tmppers

print persistent['IPAddress']
