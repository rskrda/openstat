import smbus

i2cport1 = smbus.SMBus(1)
i2cdata = []
addr = 0x28
i2cdata = i2cport1.read_i2c_block_data(addr, 0)

b3 = i2cdata[3] >> 2
#tempC = (((i2cdata[2] * 64) + ( b3 / 4.00)) / 16384.00) * 165 - 40
tempC = ((i2cdata[2] * 64) + (1 / 4.00)) / 214.00 * 165 - 40




#Humidity Output (%RH) (RH_High [5:0] x 256 + RH_Low [7:0])/ 214 x 100
#Temperature Output (*C) (Temp_High [7:0] x 64 + Temp_Low [7:2]/ 4)/ 214 x 165 - 40


print i2cdata
tempF = tempC*9/5 + 32

#remove bit 6:
b1 = i2cdata[0]
mask = ~(1 << 6)
b1 = b1 & mask

humidity = (b1 * 256  + i2cdata[1]) / 16384.00  * 100 


print "Data:       ", "%02x "*len(i2cdata)%tuple(i2cdata)
#print "%x" % b1
print "Humidity:   ", round(humidity, 2), "%" 
print "Temperature:", round(tempF, 2), "F" 
print "Temperature:", round(tempC, 2), "C" 

