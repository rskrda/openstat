//#include <glib.h>
//#include <glib/gprintf.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/*
char
adc_read(char buf) {
    char buf[64];
    return buf;
}

const char
adc_write(char buf) {
    const char *buffer;
    return buffer;
}
*/

void
sensors_ADC_init(void) {
    int file;
    char filename[16];
    const char *buffer;
    int addr = 0b01100100;        // The I2C address of the ADC (pg12 maxim 11600 guide)

    sprintf(filename,"/dev/i2c-2");
    if ((file = open(filename,O_RDWR)) < 0) {
        printf("Failed to open the bus.");
        /* ERROR HANDLING; you can check errno to see what went wrong */
        exit(1);
    }

    if (ioctl(file,I2C_SLAVE,addr) < 0) {
        printf("Failed to acquire bus access and/or talk to slave.\n");
        /* ERROR HANDLING; you can check errno to see what went wrong */
        exit(1);
    }



    char buf[1] = {0};
    float data;
    char channel;
    printf("raw data %x\n", *buf);


            //ack
            buf[0] = 0b00000001;

            if (write(file,buf,1) != 1) {
                printf("Failed to write to the i2c bus.\n");
            }



    //pg 15 of max11600 datasheet, send config or setup byte
            buf[0] = 0b10001111;

            if (write(file,buf,1) != 1) {
                printf("Failed to write to the i2c bus.\n");
            }

            //ack
            buf[0] = 0b00000001;

            if (write(file,buf,1) != 1) {
                printf("Failed to write to the i2c bus.\n");
            }

    /*
        if (read(file,buf,2) != 2) {
            printf("Failed to read from the i2c bus.\n");
            buffer = strerror(errno);
            printf(buffer);
            printf("\n\n");
        } else {
            printf("raw data %x\n", buf);
        }



            buf[0] = 0b10000000;

            if (write(file,buf,1) != 1) {
                printf("Failed to write to the i2c bus.\n");
            }
*/
    int x = 0;
    int i = 0;
    for(i; i<4; i++) {
        // Using I2C Read
        if (read(file,buf,1) == -1) {
            /* ERROR HANDLING: i2c transaction failed */
            printf("Failed to read from the i2c bus.\n");
            buffer = strerror(errno);
            printf(buffer);
            printf("\n\n");
        } else {
            printf("raw data %x\n", *buf);
            //data = (float)((buf[0] & 0b00001111)<<8)+buf[1];
            //data = data/4096*5;
            //channel = ((buf[0] & 0b00110000)>>4);
            //printf("Channel %02d Data:  %04f\n",channel,data);

    /*        buf[0] = 0b00000001;

            if (write(file,buf,1) != 1) {
                printf("Failed to write to the i2c bus.\n");
            }
*/
    
   for (x; x < sizeof(buf); x++) {
      printf("\nbuf[%d] = %d", x, buf[x]);
   }
    printf("\n");
    x = 0;

        }
        data = 0;
        channel = 0;
    }

    //unsigned char reg = 0x10; // Device register to access
    //buf[0] = reg;
    buf[0] = 0b00000001;

    if (write(file,buf,1) != 1) {
        /* ERROR HANDLING: i2c transaction failed */
        printf("Failed to write to the i2c bus.\n");
        buffer = strerror(errno);
        printf(buffer);
        printf("\n\n");
    }

}


int main() {

    sensors_ADC_init();
    return 1;
}
