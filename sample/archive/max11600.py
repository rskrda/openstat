#!/usr/bin/env python -tt
#for help see http://www.lm-sensors.org/browser/i2c-tools/trunk/py-smbus/smbusmodule.c

#if this returns a result of all zeros, do the following:
#connect +3.3v to a 5kohm resistor
#connect that resistor to AIN0
#connect a 10kom resistor to AIN0 and Ground
#  3.3v-----5kohm-----AIN0---10k0hm---Ground

import smbus, time

i2c = smbus.SMBus(2)

addr = 0x64 #(100 dec /  1100100 bin)


#i2c.write_quick #start condition
i2c.write_byte(addr, 0) #address bits
#i2c.write_quick #start condition

value = i2c.read_byte(addr)
print value

for sensors in range(0,4):
    value = i2c.read_word_data(addr, 0) ## should be 105
    print value
    time.sleep(0.1)
#   i2c.write_quick
#endfor


#write_quick Command
#Quick Command sends a single data bit from the master to the slave device (i.e., a one-bit
#write). Quick Command does not include a command byte (typically used to select a
#specific register in the slave device).
#http://developer.mbed.org/media/uploads/TC/i2c_transaction_summary.pdf



#close
