import smbus

i2cport1 = smbus.SMBus(1)
i2cdata = []
addr = 0x64
i2cdata = i2cport1.read_i2c_block_data(addr, 0)

print "Data:       ", "%02x "*len(i2cdata)%tuple(i2cdata)


i2cport2 = smbus.SMBus(2)
i2cdata = []
addr = 0x64
i2cdata = i2cport2.read_i2c_block_data(addr, 0)

print "Data:       ", "%02x "*len(i2cdata)%tuple(i2cdata)
