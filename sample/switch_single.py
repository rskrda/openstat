import Adafruit_BBIO.GPIO as GPIO
import time
import sys
 
pin = sys.argv[1]  #["P9_22"]
gpioState = int(sys.argv[2])

print pin, gpioState
GPIO.setup(pin, GPIO.OUT)
GPIO.output(pin, gpioState)


time.sleep(10.00)

GPIO.cleanup()
