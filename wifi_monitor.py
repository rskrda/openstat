#!/usr/bin/python -tt
import subprocess

def uptime_check():

    with open('/proc/uptime') as proc_uptime:
        line = proc_uptime.read().splitlines()
    proc_uptime.close()

    #parse out the uptime value
    tmp = line[0].split()
    tmp2 = tmp[0]
    uptime = float(tmp2)

    #if uptime >30 minutes, continue
    if uptime <= 1800:
        return 0
    else:
        return 1
#end uptime_check


def wireless_check():
    #find the default gw then ping it
    #if there's 1 or 0 replies, return 0 (fail)
    #2 or more is a (marginal) pass

    tmp = subprocess.check_output('ip route | grep default', shell=True) 
    tmp2 = tmp.split()
    defaultgw = tmp2[2]

    #print defaultgw

    pingcmd = "ping " + defaultgw + " -c 5 | grep received"

    tmp = subprocess.check_output(pingcmd, shell=True)
    tmp2 = tmp.split()
    #print tmp2
    
    packets_received = int(tmp2[3])

    if packets_received <= 1:
        return 0
    else:
        return 1

#end wireless_check 

def wireless_refresh():
    tmp = subprocess.check_output("/etc/init.d/networking restart", shell=True)
#end wreless_refresh

def main():
    if uptime_check():
        if wireless_check() == 0:
            print "wireless apperas down, refreshing"
            wireless_refresh()
        #else:
            #print "wireless is online"
#end main

if __name__ == '__main__':
    main()
