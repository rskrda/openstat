#!/usr/bin/python -tt
#general
import signal
import thread
#from apscheduler.scheduler import Scheduler
import time

#for tornado
import os.path
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

#tornado options
from tornado.options import define, options
define("port", default=80, help="run on the given port", type=int)

#for running commands (monitor check)
import os
os.environ["DISPLAY"] = ":0.0"

#adafruit BBIO
import sys
import time
import Adafruit_BBIO.GPIO as GPIO #digital out

#for chipcap2 access
import smbus
global i2cport1
i2cport1 = smbus.SMBus(1)

#count how long the program has been running
#GetTemp uses this to ensure it is not polling CCA2 too fast
global uptime
StartTime = time.time()

#configure GPIO pins here
global GPIODict
GPIODict = { 'HeatGPIO':'P9_22', 'FanGPIO':'P9_26', 'CoolGPIO':'P9_42', 'OBGPIO':'P9_11', 
    'Aux1GPIO':'P9_13', 'Aux2GPIO':'P9_21', 'Aux3GPIO':'P9_25', 'Aux4GPIO':'P9_41'
    } 

#initialize GPIO
for key,value in sorted(GPIODict.items()):
    GPIO.setup(value, GPIO.OUT)

#global variables
global Status
global Switch
Status = {'Heat':0, 'Cool':0, 'Fan':0, 'OB':0, 'Aux1':0, 'Aux2':0, 'Aux3':0, 'Aux4':0}
Switch = {'Heat':0, 'Cool':0, 'Fan':0, 'OB':0, 'Aux1':0, 'Aux2':0, 'Aux3':0, 'Aux4':0, 'ThermStatus':1}
#ThermStatus is a switch for the whole program. Once it is off, the program exists.
#This is done by setting the temp to 'x' instead of a number


#start functions and classes
def SetRelay(Element, Mode):

    GPIOName = Element + 'GPIO'
    global GPIODict
    global Status

    if Mode == 'On':
        GPIO.output(GPIODict[GPIOName], GPIO.HIGH)
        Status[Element] = 1
    if Mode == 'Off':
        Status[Element] = 0
        GPIO.output(GPIODict[GPIOName], GPIO.LOW)

    logger.AddLog('[' + Element.upper() + '] has been turned ' + Mode )
#end function

class TornadoLogHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('log.html', ThermLogs=logger.DisplayLog())
    #end def get
#end class

class TornadoSettingsHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('settings.html')
    #end def get
#end class


class TornadoStatsHandler(tornado.web.RequestHandler):
    global OutsideTemp

    def get(self):
        self.render('stats.html', 
        CurrTemp=GetTemp('read'),
        CurrHum=GetTemp('Humidity'), 
        OutsideTemp1=OutsideTemp,
        UserTemp=GetTemp('user'),
        Heat_Switch=Switch['Heat'],
        Cool_Switch=Switch['Cool'],
        Fan_Switch=Switch['Fan'],
        Heat_Status=Status['Heat'],
        Cool_Status=Status['Cool'],
        Fan_Status=Status['Fan']
        )
    #end def get
#end class

def GetAccessPoints():
    import subprocess 

    try:
        tmp = subprocess.check_output("/usr/bin/python /root/kstat/wifi_list_aps.py", shell=True)
        #print tmp
        global AccessPoints
        AccessPoints = []
        AccessPoints.extend(tmp.split('\n'))
        while len(AccessPoints) <= 5:
            AccessPoints.extend(" ")
        #end while
        return AccessPoints

    except subprocess.CalledProcessError as e:
        errorList = []
        errorList.extend(" Error ")
        return errorList

#end def

def SetAccessPoint(ap, enc, psk):
    import subprocess 

    cmdString = "/usr/bin/python ./wifi_config.py " + ap + " " + enc + " " + psk

    tmp = subprocess.check_output(cmdString, shell=True)
    #print tmp
    global AccessPoints
    AccessPoints = []
    AccessPoints.extend(tmp.split('\n'))
    #print AccessPoints
    #print AccessPoints[1]

    return AccessPoints
#end def

def GetAddr(addrType):
    import subprocess

    if addrType == "ip":
        try:
            tmp = subprocess.check_output('ip route | grep wlan0', shell=True) 
            tmp2 = tmp.split()
            ipaddr = tmp2[13]
            return ipaddr
        except subprocess.CalledProcessError as e:
            return "Not Connected"

    if addrType == "gw":
        try:
            tmp = subprocess.check_output('ip route | grep default', shell=True) 
            tmp2 = tmp.split()
            defaultgw = tmp2[2]
            return defaultgw
        except subprocess.CalledProcessError as e:
            return "Not Connected"

#endDef


class TornadoWifiHandler(tornado.web.RequestHandler):
    def get(self):
        import os
       
        #get the current ip address, if not connected, display that 
        #self.render('wifi.html', IPAddress=GetAddr("ip"), AccessPoints=GetAccessPoints())

        APList = GetAccessPoints()

        self.render('wifi.html', 
                   AP1=APList[1],
                   AP2=APList[2],
                   AP3=APList[3],
                   AP4=APList[4],
                   AP5=APList[5],
                   )
    #end def get

    def post(self):
        psk = self.get_argument('psk')
        apenc = self.get_argument('apenc')
        #print psk
        #print apenc
        tmp = apenc.split()
        ap = tmp[0]
        enc = tmp[1]
        
        SetAccessPoint(ap, enc, psk)
        APList = GetAccessPoints()

        self.render('wifi.html',
                   AP1=APList[1],
                   AP2=APList[2],
                   AP3=APList[3],
                   AP4=APList[4],
                   AP5=APList[5],
                   )
        #self.render('wifi.html', IPAddress=GetAddr("ip"), AccessPoints=GetAccessPoints())
    #end def post

#end class

class TornadoIndexHandler(tornado.web.RequestHandler):
    global Switch
    global Status
    
    def get(self):
        self.render('index.html',
        CurrTemp=GetTemp('read'),
        CurrHum=GetTemp('Humidity'), 
        UserTemp=GetTemp('user'),
        Heat_Switch=Switch['Heat'],
        Cool_Switch=Switch['Cool'],
        Fan_Switch=Switch['Fan'],
        Heat_Status=Status['Heat'],
        Cool_Status=Status['Cool'],
        Fan_Status=Status['Fan']
        )
    #end def
    
    def post(self):
        NewUserTemp = self.get_argument('SetTemp')


        Switch['Heat'] = 1
        Switch['Cool'] = 1
        Switch['Fan'] = 1

        try:
            self.get_argument('Heat_Switch')
        except:
            Switch['Heat'] = 0
        try:
            self.get_argument('Cool_Switch')
        except:
            Switch['Cool'] = 0
        try:
            self.get_argument('Fan_Switch')
        except:
            Switch['Fan'] = 0
#        try:
#            self.get_argument('Heat_Status')
#        except:
#            a = 1
#        try:
#            self.get_argument('Cool_Status')
#        except:
#            a = 1
#        try:
#            self.get_argument('Fan_Status')
#        except:
#            a = 1
        
        if NewUserTemp == "x":
            Switch['ThermStatus']=0
            tornado.ioloop.IOLoop.instance().stop()
        else:
            global UserTemp
            UserTemp = float(NewUserTemp)
            
            self.render('index.html',
            CurrTemp=GetTemp('read'), 
            UserTemp=GetTemp('user'),
            Heat_Switch=Switch['Heat'],
            Cool_Switch=Switch['Cool'],
            Fan_Switch=Switch['Fan'],
            Heat_Status=Status['Heat'],
            Cool_Status=Status['Cool'],
            Fan_Status=Status['Fan']
            )
           
    #end def

#end class



def InitWebServer():
    tornado.options.parse_command_line()
    app = tornado.web.Application(
        handlers=[
            (r'/', TornadoIndexHandler),
            (r'/log', TornadoLogHandler),
            (r'/settings', TornadoSettingsHandler),
            (r'/stats', TornadoStatsHandler),
            (r'/wifi', TornadoWifiHandler),
        ],
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        debug=True
    )
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
#end def InitWebServer


# next steps for this:
# return the humidity
# check how long it's been since this has been read, and provide the stale data if needed
# going with no newer than 10 seconds as the CCA2 is inaccurate at <7 seconds
def GetTemp(mode):
    global UserTemp
    try:
        UserTemp
    except:
        UserTemp = 21.0

    #enter i2c bit banging. The results are two 14 bit sections in 4 bytes:
    #
    #s s h h  h h h h  h h h h  h h h h  t t t t  t t t t  t t t t  t t x x
    #
    #those are the actual positions. S is a status always set to 10 (ignore)
    # h is 14 bit humidity, t is 14 bit temperature, x is random noise (ignore)

    global LastPoll
    global TempCelcius
    global Humidity


    # this if structure and LastPoll is designed to prevent CCA2 from being
    # polled too frequently. In testing, frequent polling leads to inaccurate
    # results which could lead to unexepcted behavior
    # the documentation states once every 7 seconds is ok, so I chose 8 seconds
    try:
        LastPoll
    except:
        LastPoll = time.time() - 9.0

    if (time.time() - LastPoll) >= 8.0:
        #init variabels
        global i2cport1
        i2cdata = []
        addr = 0x28 #cca2 defaults 0x28 on i2cbus 2
        i2cdata = i2cport1.read_i2c_block_data(addr, 0)
        
        #temperature, B2 is fine, B3 needs 2 >> bit shift, significance matters
        b3 = i2cdata[3]
        b3 = b3 & ~(1<<0)
        b3 = b3 & ~(1<<1)

        TempCelcius = ((((i2cdata[2] * 64) + ( b3 / 4.00)) / 16384.00) * 165.00) - 47
    
        #TempFarenheit = TempCelcius*9/5 + 32

        #humidity bit banging
        #remove bit 6:
        b0 = i2cdata[0]
        b0 = b0 & ~(1<<7)
        b0 = b0 & ~(1<<6)

        #b1 = i2cdata[0]
        #mask = ~(1 << 6)
        #b1 = b1 & mask
        Humidity = ((b0 * 256.00  + i2cdata[1]) / 16384.00)  * 100 

        LastPoll = time.time()
    #end if PollTime..

    #output as needed
    if mode is 'read':
        return round(TempCelcius, 2)

    if mode is 'user':
        return UserTemp

    if mode is 'Humidity':
        return round(Humidity, 2)

#end def getTemp

def heat_schedule():
    hour = int(time.strftime("%H", time.localtime()))
    global UserTemp

    #if DEBUG == 1:
    logger.AddLog('[SCHEDULE] checked at ' + str(hour))
    
    #if (hour == 6):
    #    UserTemp = 21
    #    return 0
    #if (hour == 5):
    #    SetRelay('Fan', 'On')
    #    return 0
    if (hour == 7): 
        UserTemp = 20.5 #SetRelay('Fan', 'Off')
        return 0
    if (hour == 23):
        UserTemp = 19  
        return 0
#end def heat_schedule

class LoggingClass:
    def AddLog(self, NewLog):
        try:
            self.log
        except:
            self.log = []
        self.log.append(time.ctime() + ' :: ' + NewLog)
    #end def add_log

    def DisplayLog(self):
        return self.log
    #end display_log
#end class



def Thermostat():
    #deprecatable debug
    DEBUG = 0

    #function variabels
    interval = 1 #number of idle seconds between loops
    MonitorCount = 0 #times out screen after the variable below
    MonitorCountMax = 30

    global logger 
    logger = LoggingClass()
    logger.AddLog('[MAIN->RUNNING] sampling every ' + str(interval) + ' second(s)')

    
    #scheduler
    #deprecated, using my own
    #sched = Scheduler()
    #sched.start()
    #sched.add_interval_job(heat_schedule, hours=1);
    heat_schedule()
  
    global Status
    global Switch
   
    #outside temperature
    global OutsideTemp
    OutsideTemp = 0
    #WeatherCmd = os.popen('weather -m CYLW | grep Temperature | cut -d " " -f 5')
    #OutsideTemp = WeatherCmd.read()
    WeatherCount = 0
    WeatherCountMax = 600 #get weather every 10 minutes

    while Switch['ThermStatus']:
      
        #new scheduler 
        minute = int(time.strftime("%M", time.localtime()))
        second = int(time.strftime("%S", time.localtime()))
        if minute == 0 and second < 5:
            heat_schedule()
        #end if 

        # Put the screen to sleep after a timeout period MonitorCountMax
        xsetCmd = os.popen("xset q | grep Monitor | cut -d ' ' -f 5")
        MonitorStatus = xsetCmd.read()
        #print MonitorStatus 

        if MonitorStatus == 'On\n':
            MonitorCount += 1
            if MonitorCount > MonitorCountMax:
                MonitorCount = 0
                os.system("xset dpms force off")
        # End screen sleep block


        # Get weather every 5 minutes
        WeatherCount += 1
        if WeatherCount >= WeatherCountMax:
            WeatherCmd = os.popen('weather -m CYLW | grep Temperature | cut -d " " -f 5')
            WeatherCount = 0
            OutsideTemp = WeatherCmd.read()
        #print WeatherCmd.read() 
        # end get weather

 
        #This needs to be abstracted....
        if (Switch['Fan'] == 1 and Status['Fan'] == 0):
            SetRelay('Fan', 'On')
        if (Switch['Fan'] == 0 and Status['Fan'] == 1):
            SetRelay('Fan', 'Off')
        if (Switch['Heat'] == 0 and Status['Heat'] == 1):
            SetRelay('Heat', 'Off')
        if (Switch['Cool'] == 0 and Status['Cool'] == 1):
            SetRelay('Cool', 'Off')


        #Humidity = GetTemp('Humidity')    
        
        Temperature = GetTemp('read')
        
        #if DEBUG is 1:
        #    logger.AddLog('[RUNNING] user: ' + str(GetTemp('user')) + ' read: ' + str(Temperature))

        if (Switch['Fan'] == 1 and Status['Fan'] == 0):
            SetRelay('Fan', 'On')
            Status['Fan'] = 1
        if (Switch['Fan'] == 0 and Status['Fan'] == 1):
            SetRelay('Fan', 'Off')
            Status['Fan'] = 0
       
        if (Switch['Heat'] == 1 and Switch['Cool'] == 0):
            # the co-efficient on works as 0.9 in the winter
            if ((Temperature < ( GetTemp('user') - 0.3)) and Status['Heat'] == 0):
                SetRelay('Heat', 'On')
                Status['Heat'] = 1
                heat_temp_start = Temperature
                heat_monitor_time = 0
            #end if < Temperature...

            # the co-efficient off works as 0.4 in winter
            if ((Temperature > ( GetTemp('user') + 0.3)) and Status['Heat'] == 1):
                SetRelay('Heat', 'Off')
                Status['Heat'] = 0
                heat_monitor_time = 0
            #end if > Temperature...

            #exception handler, cycle system if blowing cold air
            if (Status['Heat'] == 1):
                heat_monitor_time += interval
                if (heat_monitor_time >= 300 and (heat_temp_start >= Temperature)):
                    logger.AddLog('[EXCEPTION] several minutes have past and no heat increase detected')
                    logger.AddLog('[EXCEPTION] user: ' + str(GetTemp('user')) + ' first called heat at: ' + str( round(heat_temp_start, 1) ) + ' heat now at: ' + str( round(Temperature, 1) ))
                    SetRelay('Heat', 'Off')
                    Status['Heat'] = 0
                    heat_monitor_time = 0
                    time.sleep(180)
                #end if heat_mon...
            #end if g_heat...    


        if (Switch['Heat'] == 0 and Switch['Cool'] == 1):
            # the co-efficient on works as 0.9 in the winter
            if ((Temperature > ( GetTemp('user') + 0.4)) and Status['Cool'] == 0):
                SetRelay('Cool', 'On')
                Status['Cool'] = 1
            #end if < Temperature...
            if ((Temperature < ( GetTemp('user') - 0.4)) and Status['Cool'] == 1):
                SetRelay('Cool', 'Off')
                Status['Cool'] = 0
            #end if > Temperature...



        time.sleep(interval)

    #end while


    #the heat on/off needs to be a function, generalized for FAN and COOL

    SetRelay('Heat', 'Off')
    SetRelay('Fan', 'Off')
    #SetRelay('Cool', 'Off')

    GPIO.cleanup()
    #sys.exit(0)
#end thermostat

def main():
    try:
        thread.start_new_thread(Thermostat, ()) #thermostat
        thread.start_new_thread(InitWebServer, ()) #tornado web server
    except KeyboardInterrupt:
        print "CTRL + C, shutting down"

    #simulation of main's while
    while Switch['ThermStatus']:
        time.sleep(1)
#end main


if __name__ == '__main__':
    main()
