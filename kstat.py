#!/usr/bin/python -tt
#general
import signal
import thread
#from apscheduler.scheduler import Scheduler
import time

#for tornado
import os.path
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

#tornado options
from tornado.options import define, options
define("port", default=80, help="run on the given port", type=int)

#for running commands (monitor check)
import os
os.environ["DISPLAY"] = ":0.0"
import subprocess 
#adafruit BBIO
import sys
import time
import Adafruit_BBIO.GPIO as GPIO #digital out

#for chipcap2 access
import smbus
global i2cport1
i2cport1 = smbus.SMBus(2)

#count how long the program has been running
#GetTemp uses this to ensure it is not polling CCA2 too fast
global uptime
StartTime = time.time()

#configure GPIO pins here
global GPIODict
GPIODict = { 'HeatGPIO':'P9_22', 'FanGPIO':'P9_26', 'CoolGPIO':'P9_42', 'OBGPIO':'P9_11', 
    'Aux1GPIO':'P9_13', 'Aux2GPIO':'P9_21', 'Aux3GPIO':'P9_25', 'Aux4GPIO':'P9_41'
    } 

#initialize GPIO
for key,value in sorted(GPIODict.items()):
    GPIO.setup(value, GPIO.OUT)

#global variables and settings
global settings
import pickle
try: 
    f = open('/root/kstat/kstat.pckl')
    settings = pickle.load(f)
    f.close()
except IOError:
    #no settings file existed, loading defaults:
    settings = {
        "celcius": 1,
        "day_heat_temp": 21,
        "night_heat_temp": 19,
        "heat_delta_below": 0.3,
        "heat_delta_above": 0.3,
        "day_cool_temp": 26,
        "night_cool_temp": 30,
        "night_start": 22,
        "night_end": 7,
        "heat_delta_mon": 0,
        "cool_delta_mon": 0,
        "digital_temp_calib": 0,
        "digital_hum_calib": 0,
        "sensors_read": 0,
        "sensor_log_interval": 600,
        "heat_switch": 0,
        "cool_switch": 0,
        "fan_switch": 0,
        "fan_schedule_start": 0,
        "fan_schedule_stop": 0,
        "airport_code": "LAX",
    }
#end settings

global Status
global Switch
Status = {'Heat':0, 'Cool':0, 'Fan':0, 'OB':0, 'Aux1':0, 'Aux2':0, 'Aux3':0, 'Aux4':0}
Switch = {'Heat':0, 'Cool':0, 'Fan':0, 'OB':0, 'Aux1':0, 'Aux2':0, 'Aux3':0, 'Aux4':0, 'ThermStatus':1}
#initialize settings based on stored config
Switch['Heat'] = settings['heat_switch']
Switch['Cool'] = settings['cool_switch']
Switch['Fan']  = settings['fan_switch']

#start functions and classes
def SetRelay(Element, Mode):

    GPIOName = Element + 'GPIO'
    global GPIODict
    global Status

    if Mode == 'On':
        GPIO.output(GPIODict[GPIOName], GPIO.HIGH)
        Status[Element] = 1
    if Mode == 'Off':
        Status[Element] = 0
        GPIO.output(GPIODict[GPIOName], GPIO.LOW)

    logger.AddLog('[' + Element.upper() + '] has been turned ' + Mode )
#end function

class TornadoLogHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('log.html', ThermLogs=logger.DisplayLog())
    #end def get
#end class

class TornadoRebootHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Rebooting")
        os.popen("reboot")
       

class TornadoSettingsHandler(tornado.web.RequestHandler):

    def get(self):
        global settings

        self.render('settings.html',
            celcius = settings['celcius'],
            day_heat_temp = settings['day_heat_temp'],
            night_heat_temp = settings['night_heat_temp'],
            day_cool_temp = settings['day_cool_temp'],
            night_cool_temp = settings['night_cool_temp'],
            night_start = settings['night_start'],
            night_end = settings['night_end'],
            heat_delta_mon = settings['heat_delta_mon'],
            cool_delta_mon = settings['cool_delta_mon'],
            digital_temp_calib = settings['digital_temp_calib'],
            digital_hum_calib = settings['digital_hum_calib'],
            sensors_read = settings['sensors_read'],
            sensor_log_interval = settings['sensor_log_interval'],
            heat_delta_above = settings['heat_delta_above'],
            heat_delta_below = settings['heat_delta_below'],
            fan_schedule_start = settings['fan_schedule_start'],
            fan_schedule_stop = settings['fan_schedule_stop'],
            airport_code = settings['airport_code'],
            )

    def post(self):
        global settings
        global logger
        global Switch

        settings['celcius'] = int(self.get_argument('celcius'))
        settings['day_heat_temp'] = float(self.get_argument('day_heat_temp'))
        settings['night_heat_temp'] = float(self.get_argument('night_heat_temp'))
        settings['day_cool_temp'] = float(self.get_argument('day_cool_temp'))
        settings['night_cool_temp'] = float(self.get_argument('night_cool_temp'))
        settings['night_start'] = int(self.get_argument('night_start'))
        settings['night_end'] = int(self.get_argument('night_end'))
        settings['heat_delta_mon'] = int(self.get_argument('heat_delta_mon'))
        settings['cool_delta_mon'] = int(self.get_argument('cool_delta_mon'))
        settings['digital_temp_calib'] = float(self.get_argument('digital_temp_calib'))
        settings['digital_hum_calib'] = float(self.get_argument('digital_hum_calib'))
        settings['sensors_read'] = int(self.get_argument('sensors_read'))
        settings['sensor_log_interval'] = int(self.get_argument('sensor_log_interval'))
        settings['heat_delta_above'] = float(self.get_argument('heat_delta_above'))
        settings['heat_delta_below'] = float(self.get_argument('heat_delta_below'))
        settings['fan_schedule_start'] = int(self.get_argument('fan_schedule_start'))
        settings['fan_schedule_stop'] = int(self.get_argument('fan_schedule_stop'))
        settings['airport_code'] = self.get_argument('airport_code')

        try:
            settings['heat_switch'] = Switch['Heat']
            settings['cool_switch'] = Switch['Cool']
            settings['fan_switch'] = Switch['Fan']
            f = open('/root/kstat/kstat.pckl', 'w')
            pickle.dump(settings, f)
            f.close()
            logger.AddLog('[Settings] Saved Sucessfully')
        except IOError:
            logger.AddLog('[Settings] Could not save settings')

        self.render('settings.html',
            celcius = settings['celcius'],
            day_heat_temp = settings['day_heat_temp'],
            night_heat_temp = settings['night_heat_temp'],
            day_cool_temp = settings['day_cool_temp'],
            night_cool_temp = settings['night_cool_temp'],
            night_start = settings['night_start'],
            night_end = settings['night_end'],
            heat_delta_mon = settings['heat_delta_mon'],
            cool_delta_mon = settings['cool_delta_mon'],
            digital_temp_calib = settings['digital_temp_calib'],
            digital_hum_calib = settings['digital_hum_calib'],
            sensors_read = settings['sensors_read'],
            sensor_log_interval = settings['sensor_log_interval'],
            heat_delta_above = settings['heat_delta_above'],
            heat_delta_below = settings['heat_delta_below'],
            airport_code = settings['airport_code'],
            fan_schedule_start = settings['fan_schedule_start'],
            fan_schedule_stop = settings['fan_schedule_stop'],
            )


    #end def get
#end class
    


class TornadoStatsHandler(tornado.web.RequestHandler):
    global OutsideTemp
    global settings

    def get(self):
        self.render('stats.html', 
        CurrTemp=GetTemp('read'),
        CurrHum=GetTemp('Humidity'), 
        OutsideTemp1=OutsideTemp,
        UserTemp=GetTemp('user'),
        Heat_Switch=Switch['Heat'],
        Cool_Switch=Switch['Cool'],
        Fan_Switch=Switch['Fan'],
        Heat_Status=Status['Heat'],
        Cool_Status=Status['Cool'],
        Fan_Status=Status['Fan'],
        Celcius=settings['celcius'],
        )
    #end def get
#end class

def GetAccessPoints():

    try:
        tmp = subprocess.check_output("/usr/bin/python /root/kstat/wifi_list_aps.py", shell=True)
        #print tmp
        global AccessPoints
        AccessPoints = []
        AccessPoints.extend(tmp.split('\n'))
        while len(AccessPoints) <= 5:
            AccessPoints.extend(" ")
        #end while
        return AccessPoints

    except subprocess.CalledProcessError as e:
        errorList = []
        errorList.extend(" Error ")
        return errorList

#end def

def SetAccessPoint(ap, enc, psk):

    cmdString = "/usr/bin/python /root/kstat/wifi_config.py " + ap + " " + enc + " " + psk

    tmp = subprocess.check_output(cmdString, shell=True)
    #print tmp
    tmp = subprocess.check_output("/etc/init.d/networking restart", shell=True)

    global AccessPoints
    AccessPoints = []
    AccessPoints.extend(tmp.split('\n'))
    #print AccessPoints
    #print AccessPoints[1]

    return AccessPoints
#end def

def GetAddr(addrType):

    if addrType == "ip":
        try:
            tmp = subprocess.check_output('ip route | grep wlan0', shell=True) 
            tmp2 = tmp.split()
            ipaddr = tmp2[13]
            return ipaddr
        except subprocess.CalledProcessError as e:
            return "Not Connected"

    if addrType == "gw":
        try:
            tmp = subprocess.check_output('ip route | grep default', shell=True) 
            tmp2 = tmp.split()
            defaultgw = tmp2[2]
            return defaultgw
        except subprocess.CalledProcessError as e:
            return "Not Connected"

#endDef


class TornadoWifiHandler(tornado.web.RequestHandler):
    def get(self):
       
        #get the current ip address, if not connected, display that 
        #self.render('wifi.html', IPAddress=GetAddr("ip"), AccessPoints=GetAccessPoints())

        #APList = GetAccessPoints()

        self.render('wifi.html', IPAddr=GetAddr("ip"))
    #end def get

    def post(self):
        psk = self.get_argument('psk')
        apenc = self.get_argument('apenc')
        ap = apenc
        enc = "WPA"
        
        SetAccessPoint(ap, enc, psk)
        #APList = GetAccessPoints()

        self.render('wifi.html', IPAddr=GetAddr("ip"))
        #self.render('wifi.html', IPAddress=GetAddr("ip"), AccessPoints=GetAccessPoints())
    #end def post

#end class

class TornadoIndexHandler(tornado.web.RequestHandler):
    global Switch
    global Status
    
    def get(self):
        self.render('index.html',
        CurrTemp=GetTemp('read'),
        CurrHum=GetTemp('Humidity'), 
        UserTemp=GetTemp('user'),
        Heat_Switch=Switch['Heat'],
        Cool_Switch=Switch['Cool'],
        Fan_Switch=Switch['Fan'],
        Heat_Status=Status['Heat'],
        Cool_Status=Status['Cool'],
        Fan_Status=Status['Fan']
        )
    #end def
    
    def post(self):
        global settings

        if settings['celcius'] == 1:
            temp = float(self.get_argument('SetTemp'))
        else:
            temp  = round((float(self.get_argument('SetTemp')) - 32) / 1.8, 2)

        hour = int(time.strftime("%H", time.localtime()))
        if hour >= settings['night_start'] or hour < settings['night_end']:
            settings['night_heat_temp'] = temp
        else:
            settings['day_heat_temp'] = temp

        Switch['Heat'] = 1
        Switch['Cool'] = 1
        Switch['Fan'] = 1

        try:
            self.get_argument('Heat_Switch')
        except:
            Switch['Heat'] = 0
        try:
            self.get_argument('Cool_Switch')
        except:
            Switch['Cool'] = 0
        try:
            self.get_argument('Fan_Switch')
        except:
            Switch['Fan'] = 0
#        try:
#            self.get_argument('Heat_Status')
#        except:
#            a = 1
#        try:
#            self.get_argument('Cool_Status')
#        except:
#            a = 1
#        try:
#            self.get_argument('Fan_Status')
#        except:
#            a = 1
        
        #global UserTemp
        #UserTemp = float(NewUserTemp)
        
        self.render('index.html',
        CurrTemp=GetTemp('read'), 
        UserTemp=GetTemp('user'),
        Heat_Switch=Switch['Heat'],
        Cool_Switch=Switch['Cool'],
        Fan_Switch=Switch['Fan'],
        Heat_Status=Status['Heat'],
        Cool_Status=Status['Cool'],
        Fan_Status=Status['Fan']
        )
           
    #end def

#end class



def InitWebServer():
    tornado.options.parse_command_line()
    app = tornado.web.Application(
        handlers=[
            (r'/', TornadoIndexHandler),
            (r'/log', TornadoLogHandler),
            (r'/settings', TornadoSettingsHandler),
            (r'/stats', TornadoStatsHandler),
            (r'/wifi', TornadoWifiHandler),
            (r'/reboot', TornadoRebootHandler),
        ],
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        debug=True
    )
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
#end def InitWebServer

def sensor_read(sensor_num):

    global settings
    #"usage: " + sys.argv[0] + " <int>"
    #"int is between 0 and 7 indicating the number of sensors you wish to read from"

    if sensor_num < 1:
        print "input must be above zero"
        return -1
    if sensor_num > 8:
        print "input must be equal or below 8"
        return -1

    readings = []
    i2caddr = 72
    ain = 0
    for sensors in range(0, sensor_num):
        if sensors == 4:
            i2caddr = 73
            ain = 0
        sensor_cmd = "/root/kstat/sample/ads1015/ads1015 " + str(ain) + " " + str(i2caddr) + " 2"
        #print sensor_cmd
        tmp = subprocess.check_output(sensor_cmd, shell=True)
        tmp = tmp.rstrip()
        tmp2 = sensor_tconversion(float(tmp))
        if settings['celcius'] == 0:
            tmp2 = round(tmp2 * 1.8 + 32, 2) #C  x  9/5 + 32 = F
        #print tmp2
        ain = ain + 1
        readings.append(tmp2)
    #end for
    return readings
#end def senso...

def sensor_tconversion(Vout):

    import math

    if Vout <= 0.0:
        return -40.00 #else divide by zero

    Vin = 3.3 #vdd, default
    c1 = 0 #manual correction if needed
    R2 = 9920 #10kohm (+/- 0.1 ohm) R2
    T0 = 298.15 #25*C in Kelvin (273.15+25)
    Beta = 3977.00 #from thermistor datasheet

    #voltage to resistance
    R1 = (R2 * Vin / Vout) - R2

    #formula from http://psas.pdx.edu/RocketScience/Thermistors.pdf
    temp = (Beta * T0) / (Beta + T0 * math.log(R1/R2))

    #print "input voltage at " + sys.argv[1] + " volts"
    #print "Resistor 1 is calculated to be " + str(R1) + " ohms"
    #print "thermistor temperature is " + str((temp - 273.15 + c1)) + " celcius"
    temp = temp - 273.15 + c1
    if temp <= 0.0:
        return -40.00
    return round((temp), 2)
#end def senso...

def GetTemp(mode):
    global settings
    global UserTemp
    try:
        UserTemp
    except:
        UserTemp = settings['day_heat_temp']

    global LastPoll
    global TempCelcius
    global Humidity

    # this if structure and LastPoll is designed to prevent CCA2 from being
    # polled too frequently. In testing, frequent polling leads to inaccurate
    # results which could lead to unexepcted behavior
    # the documentation states once every 7 seconds is ok, so I chose 8 seconds
    try:
        LastPoll
    except:
        LastPoll = time.time() - 9.0

    if (time.time() - LastPoll) >= 8.0:
        #init variabels
        global i2cport1
        i2cdata = []
        addr = 0x28 #cca2 defaults 0x28 on i2cbus 2
        i2cdata = i2cport1.read_i2c_block_data(addr, 0)
        
        #The results are two 14 bit sections in 4 bytes:
        #
        #s s h h  h h h h  h h h h  h h h h  t t t t  t t t t  t t t t  t t x x
        #
        #those are the actual positions. S is a status always set to 10 (ignore)
        # h is 14 bit humidity, t is 14 bit temperature, x is random noise (ignore)
        
        #temperature, B2 is fine, B3 needs 2 >> bit shift, significance matters
        b3 = i2cdata[3]
        b3 = b3 & ~(1<<0)
        b3 = b3 & ~(1<<1)
        
        TempCelcius = ((((i2cdata[2] * 64) + ( b3 / 4.00)) / 16384.00) * 165.00) - 40 - settings['digital_temp_calib']
        
        #TempFarenheit = TempCelcius*9/5 + 32
        
        #humidity, remove bit 6:
        b0 = i2cdata[0]
        b0 = b0 & ~(1<<7)
        b0 = b0 & ~(1<<6)
        
        Humidity = ((b0 * 256.00  + i2cdata[1]) / 16384.00)  * 100  + settings['digital_hum_calib']

        LastPoll = time.time()
    #end if PollTime..

    #output as needed
    if mode is 'read':
        return round(TempCelcius, 2)

    if mode is 'user':
        hour = int(time.strftime("%H", time.localtime()))
        if hour >= settings['night_start'] or hour < settings['night_end']:
            return settings['night_heat_temp']
        else:
            return settings['day_heat_temp']
        #return settings['day_heat_temp']
        #return UserTemp

    if mode is 'Humidity':
        return round(Humidity, 2)

#end def getTemp

def heat_schedule():
    hour = int(time.strftime("%H", time.localtime()))
    global UserTemp
    global settings

    #if DEBUG == 1:
    logger.AddLog('[SCHEDULE] checked at ' + str(hour))
    
    #if (hour == 6):
    #    UserTemp = 21
    #    return 0
    #if (hour == 5):
    #    SetRelay('Fan', 'On')
    #    return 0
    if (hour == settings['night_end']): 
        UserTemp = settings['day_heat_temp']
        #UserTemp = 20.5 #SetRelay('Fan', 'Off')
        return 0
    if (hour == settings['night_start']):
        UserTemp = settings['night_heat_temp'] 
        #UserTemp = 19  
        return 0
    if (hour == 1):
        settings['heat_switch'] = Switch['Heat']
        settings['cool_switch'] = Switch['Cool']
        settings['fan_switch'] = Switch['Fan']
        f = open('/root/kstat/kstat.pckl', 'w')
        pickle.dump(settings, f)
        f.close()
        logger.AddLog('[Settings] Saved Sucessfully automatically at midnight')



#end def heat_schedule

class LoggingClass:
    def AddLog(self, NewLog):
        try:
            self.log
        except:
            self.log = []
        self.log.append(time.ctime() + ' :: ' + NewLog)
    #end def add_log

    def DisplayLog(self):
        return self.log
    #end display_log
#end class



def Thermostat():
    #deprecatable debug
    DEBUG = 0

    #function variabels
    interval = 1 #number of idle seconds between loops
    MonitorCount = 0 #times out screen after the variable below
    MonitorCountMax = 30

    global logger 
    logger = LoggingClass()
    logger.AddLog('[MAIN->RUNNING] sampling every ' + str(interval) + ' second(s)')

    
    #scheduler
    #deprecated, using my own
    #sched = Scheduler()
    #sched.start()
    #sched.add_interval_job(heat_schedule, hours=1);
    heat_schedule()

    global settings  
    global Status
    global Switch
   
    #outside temperature
    global OutsideTemp
    OutsideTemp = 0
    #WeatherCmd = os.popen('weather -m CYLW | grep Temperature | cut -d " " -f 5')
    #OutsideTemp = WeatherCmd.read()
    WeatherCount = 0
    WeatherCountMax = 900 #get weather every 15 minutes (often the fastest
    sensor_log_timer = 0

    while Switch['ThermStatus']:
      
        #new scheduler 
        minute = int(time.strftime("%M", time.localtime()))
        second = int(time.strftime("%S", time.localtime()))
        if minute == 0 and second < 5:
            heat_schedule()
        #end if 

        # Put the screen to sleep after a timeout period MonitorCountMax
        xsetCmd = os.popen("xset q | grep Monitor | cut -d ' ' -f 5")
        MonitorStatus = xsetCmd.read()
        #print MonitorStatus 

        if MonitorStatus == 'On\n':
            MonitorCount += 1
            if MonitorCount > MonitorCountMax:
                MonitorCount = 0
                os.system("xset dpms force off")
        # End screen sleep block


        # super dangerous, could be used to run arbitrary commands
        # string checking is needed
        # Get weather every 5 minutes
        if settings['airport_code'] == "":
            OutsideTemp = 0
        else:
            WeatherCount += 1
            if WeatherCount >= WeatherCountMax:
                tmpcmd = 'weather -m ' + settings['airport_code'] + ' | grep Temperature | cut -d " " -f 5'
                #print tmpcmd
                WeatherCmd = os.popen(tmpcmd)
                WeatherCount = 0
                OutsideTemp = WeatherCmd.read()
            #end  if Weather
        # end get weather

 
        #This needs to be abstracted....
        if (Switch['Fan'] == 1 and Status['Fan'] == 0):
            SetRelay('Fan', 'On')
        if (Switch['Fan'] == 0 and Status['Fan'] == 1):
            SetRelay('Fan', 'Off')
        if (Switch['Heat'] == 0 and Status['Heat'] == 1):
            SetRelay('Heat', 'Off')
        if (Switch['Cool'] == 0 and Status['Cool'] == 1):
            SetRelay('Cool', 'Off')


        #Humidity = GetTemp('Humidity')    
        
        Temperature = GetTemp('read')
        
        #if DEBUG is 1:
        #    logger.AddLog('[RUNNING] user: ' + str(GetTemp('user')) + ' read: ' + str(Temperature))

        #fan controll section
        if (Switch['Fan'] == 1 and Status['Fan'] == 0):
            SetRelay('Fan', 'On')
            Status['Fan'] = 1
        if (Switch['Fan'] == 0 and Status['Fan'] == 1):
            SetRelay('Fan', 'Off')
            Status['Fan'] = 0
      
        #heating control section 
        if (Switch['Heat'] == 1 and Switch['Cool'] == 0):
            if ((Temperature < ( GetTemp('user') - settings['heat_delta_below'])) and Status['Heat'] == 0):
                SetRelay('Heat', 'On')
                Status['Heat'] = 1
                heat_temp_start = Temperature
                heat_monitor_time = 0
            #end if < Temperature...

            if ((Temperature > ( GetTemp('user') + settings['heat_delta_above'])) and Status['Heat'] == 1):
                SetRelay('Heat', 'Off')
                Status['Heat'] = 0
                heat_monitor_time = 0
            #end if > Temperature...

            if settings['heat_delta_mon'] == 1:
                #exception handler, cycle system if blowing cold air
                if (Status['Heat'] == 1):
                    heat_monitor_time += interval
                    if (heat_monitor_time >= 300 and (heat_temp_start >= Temperature)):
                        logger.AddLog('[EXCEPTION] several minutes have past and no heat increase detected')
                        logger.AddLog('[EXCEPTION] user: ' + str(GetTemp('user')) + ' first called heat at: ' + str( round(heat_temp_start, 1) ) + ' heat now at: ' + str( round(Temperature, 1) ))
                        SetRelay('Heat', 'Off')
                        Status['Heat'] = 0
                        heat_monitor_time = 0
                        time.sleep(180)
                    #end if heat_mon...
                #end if settings['heat...
            #end if g_heat...    

        #cooling control section
        if (Switch['Heat'] == 0 and Switch['Cool'] == 1):
            # the co-efficient on works as 0.9 in the winter
            if ((Temperature > ( GetTemp('user') + 0.4)) and Status['Cool'] == 0):
                SetRelay('Cool', 'On')
                Status['Cool'] = 1
            #end if < Temperature...
            if ((Temperature < ( GetTemp('user') - 0.4)) and Status['Cool'] == 1):
                SetRelay('Cool', 'Off')
                Status['Cool'] = 0
            #end if > Temperature...

        #sensors block
        if settings['sensors_read'] > 0:
            if sensor_log_timer >= settings['sensor_log_interval']:
                result = sensor_read(settings['sensors_read'])
                logger.AddLog('[Sensors] output: ' + str(result))
                sensor_log_timer = 0
            sensor_log_timer = sensor_log_timer + 1

        time.sleep(interval)
    #end while

    #cleanup on loop-ending:
    SetRelay('Heat', 'Off')
    SetRelay('Fan', 'Off')
    SetRelay('Cool', 'Off')

    GPIO.cleanup()
    #sys.exit(0)
#end thermostat

def main():
    try:
        thread.start_new_thread(Thermostat, ()) #thermostat
        thread.start_new_thread(InitWebServer, ()) #tornado web server
    except KeyboardInterrupt:
        print "CTRL + C, shutting down"

    #simulation of main's while
    while Switch['ThermStatus']:
        time.sleep(1)
#end main


if __name__ == '__main__':
    main()
