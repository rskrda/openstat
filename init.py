#!/usr/bin/python -tt
import time

#for running commands (monitor check)
import os
import sys
import time
import subprocess

def InitTasks():


    #print 'init lcd'
    lcd = 4.3
    auxinput = 1

    if lcd == 4.3:
        os.system('echo nhd-4.3-480272ef-00A0.dtb0 >> /sys/devices/bone_capemgr.9/slots')
    elif lcd == 7.0:
        os.system('echo nhd-7.0-800480mf-atxi-t-1.dtbo >> /sys/devices/bone_capemgr.9/slots')
    time.sleep(1)

    if auxinput:
        os.system('echo BB-I2C1 > /sys/devices/bone_capemgr.9/slots')


    #print 'init X'
    os.system('startx &')
    time.sleep(20)

    #print 'remove mouse'
    os.system("unclutter -display :0 -idle 0 &")


    #print 'starting main process'
    os.system('/usr/bin/python /root/kstat/kstat.py &')


    #print 'starting browser'
    os.system('x-www-browser http://localhost --display=:0 &')
    time.sleep(15)
  
    #print 'fullscreen browser'
    os.system('DISPLAY=:0 xdotool getactivewindow key F11 &')
    #time.sleep(5)
    #os.system('DISPLAY=:0 xdotool getactivewindow key F5 &')

    time.sleep(5)


    #turn off leds
    #mmc0
    os.system('echo none > /sys/class/leds/beaglebone\:green\:usr1/trigger')

    #cpu0
    os.system('echo none > /sys/class/leds/beaglebone\:green\:usr2/trigger')

    #heartbeat
    os.system('echo none > /sys/class/leds/beaglebone\:green\:usr0/trigger')


#end InitTasks

def main():
    InitTasks()
#end main


if __name__ == '__main__':
    main()
