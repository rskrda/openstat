#! /usr/bin/env python

#configuring this file (http://www.gc-linux.org/wiki/WL:Wifi_Configuration)
#usage:
#<script.py> accesspoint_ssid enc_type psk
#example:
#wifi_config.py mywifi WPA mypassword

def main():

    import sys
    import subprocess

    if len(sys.argv) != 4:
        raise Exception("usage: wifi_config.py ssid enctype psk")
        return

    ap = sys.argv[1]
    enc = sys.argv[2]
    psk = sys.argv[3]


    #wep
    #wireless-essid blah
    #wireless-key

    #wpa and wpa2
    #wpa-ssid
    #wpa-psk

    #no enc
    #wireless-essid blah

    file_list = []

    file_list.append('# This file describes the network interfaces available on your system')
    file_list.append('# and how to activate them. For more information, see interfaces(5).')
    file_list.append('')
    file_list.append('# The loopback network interface')
    file_list.append('auto lo')
    file_list.append('iface lo inet loopback')
    file_list.append('')
    file_list.append('# WiFi Example')
    file_list.append('auto wlan0')
    file_list.append('iface wlan0 inet dhcp')
    file_list.append('    wpa-ssid ' + ap )
    file_list.append('    wpa-psk  ' + psk )
    file_list.append('')
    file_list.append('# Ethernet/RNDIS gadget (g_ether)')
    file_list.append('# ... or on host side, usbnet and random hwaddr')
    file_list.append('# Note on some boards, usb0 is automaticly setup with an init script')
    file_list.append('# in that case, to completely disable remove file [run_boot-scripts] from the boot partition')
    file_list.append('iface usb0 inet static')
    file_list.append('    address 192.168.7.2')
    file_list.append('    netmask 255.255.255.0')
    file_list.append('    network 192.168.7.0')
    file_list.append('    gateway 192.168.7.1')

    #print file_list
    interface_filepath = '/etc/network/interfaces'

    interface_file = open(interface_filepath,'w')

    for line in file_list:
      interface_file.write('%s\n' % line)

    interface_file.close

    #print "refreshing wireless"
    #tmp = subprocess.check_output("/etc/init.d/networking restart", shell=True)
    #tmp = subprocess.check_output("/sbin/wpa_cli reconfigure", shell=True)
    #tmp = subprocess.check_output("/sbin/ifup wlan0", shell=True)

    #run these commands after:
    #wpa_cli reconfigure
    #ifup wlan0
    #end def

if __name__ == "__main__":
    main()
